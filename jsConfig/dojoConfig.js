/**
 * Created by Solomka on 30.10.2016.
 */

/* dojo custom config
 Instead of using data-dojo-config, we're creating a dojoConfig object *before* we load dojo.js;
 they're functionally identical, it's just easier to read this approach with a larger configuration. */
var dojoConfig = {
    baseUrl: "js/",
    isDebug: true,
    async: true,
    parseOnLoad: true,
    packages: [
        {name: "dojo", location: "dojo-release-1.11.2/dojo"},
        {name: "dijit", location: "dojo-release-1.11.2/dijit"},
        {name: "dojox", location: "dojo-release-1.11.2/dojox"},
        {name: "core", location: "core"},
        {name: "libraries", location: "libraries"}
    ]
};


