/**
 * Created by Solomka on 16.11.2016.
 */

/**
 * dojo memory store
 */
define([
    "dojo/_base/declare",
    "dojo/ready",
    "dojo/request",
    "dojo/_base/lang",
    "dojo/on",
    "dojo/dom",
    "dojo/dom-style",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/_base/array",
    "dojo/store/Memory",


    "dojo/domReady!"

], function (declare, ready, request, lang, on, dom, domStyle, domClass, domConstruct, arrayUtil, Memory) {


    dojo.global.Storage = {

        memoryStore: null,

        init: function () {

            this.memoryStore = new Memory();

        }


    };

    ready(function () {
        Storage.init();
    });

    return dojo.global.Storage;
});

