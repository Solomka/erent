/**
 * Created by Solomka on 29.11.2016.
 */
/**
 * Created by marvich on 17.11.2016.
 */

define([
    "dojo/_base/declare",
    "dojo/request",
    "dojo/_base/lang",
    "dojo/dom-style",
    "dojo/on",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",

    "dojo/text!./templates/AddNewUserGoodWidget.html",

    "dijit/form/CheckBox"

], function (declare, request, lang, domStyle, on, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: template,

        parentWidget: {},

        childUserGoodsWidgets: [],

        postCreate: function () {
        }
    });
});


