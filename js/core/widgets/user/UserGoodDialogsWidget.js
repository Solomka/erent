/**
 * Created by Solomka on 03.12.2016.
 */

define([
    "dojo/_base/declare",
    "dojo/request",
    "dojo/_base/lang",
    "dojo/dom-style",
    "dojo/on",
    "dojo/dom",
    "dojo/query",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",



    "dojo/text!./templates/UserGoodDialogsWidget.html"

], function (declare, request, lang, domStyle, on, dom, query, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: template,
        parentWidget: {},
       // childEditWidget: {},

        dialogs: [],
        _setDialogsAttr: function (dialogs) {
            this._set("dialogs", dialogs);
        },


        postCreate: function () {
            // alert("Profile");

        },

        startup: function () {

        },

        /*
        editProfile: function () {
            domStyle.set(this.profileNode, "display", "none");
            var widget = new ProfileEditWidget().placeAt(this.profileEditNode);
            childEditWidget = widget;
        }*/


    });
});



