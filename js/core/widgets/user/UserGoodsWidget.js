/**
 * Created by marvich on 17.11.2016.
 */

define([
    "dojo/_base/declare",
    "dojo/request",
    "dojo/_base/lang",
    "dojo/dom-style",
    "dojo/on",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "core/widgets/user/UserGoodWidget",
    "core/widgets/categories/CategoryFilterWidget",

    "dojo/text!./templates/UserGoodsWidget.html",

    "dijit/form/CheckBox"

], function (declare, request, lang, domStyle, on, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, UserGoodWidget, CategoryFilterWidget, template) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: template,

        parentWidget: {},

        childUserGoodsWidgets: [],

        goods: [],
        _setGoodsAttr: function (goods) {
            this._set("goods", goods);
        },

        postCreate: function () {
            var userGoods = this.goods;
            var widget = {};
            var params = {};

            for (var i in userGoods) {

                params = {
                    good: userGoods[i],
                    parentWidget: this
                };
                widget = new UserGoodWidget(params).placeAt(this.userGoods);
                this.childUserGoodsWidgets.push(widget);

            }

            // add by categories filter
            this._showCategoriesFilter();
        },

        _showCategoriesFilter: function () {
            var widget = {};
            var params = {};

            params = {
                parentWidget: this
            };

            widget = new CategoryFilterWidget(params).placeAt(this.categoryFilter);
            this.categoriesFilterWidget = widget;
        },

        /**
         * Show items only with new messages
         */
        showGoodsWithNewM: function () {
            if (!!(this.goodsWithNewMChecker.get("checked"))) {
                for (var i in this.childUserGoodsWidgets) {
                    var userGoodWidget = this.childUserGoodsWidgets[i];
                    if (userGoodWidget.baseClass == 'absent-msgs') {
                        domStyle.set(userGoodWidget.domNode, "display", "none");
                    }
                }

            } else {
                for (var i in this.childUserGoodsWidgets) {
                    var userGoodWidget = this.childUserGoodsWidgets[i];
                    domStyle.set(userGoodWidget.domNode, "display", "initial");
                }


            }
        }
    });
});

