/**
 * Created by marvich on 17.11.2016.
 */
define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom",
    "dojo/dom-style",
    "dojo/on",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",

    "dojo/text!./templates/UserGoodWidget.html"

], function (declare, lang, dom, domStyle, on, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: template,
        baseClass: "userGoodWidget",

        parentWidget: {},

        defaultGoodPhoto: require.toUrl("image/headPhones.jpg"),


        good: {},
        _setGoodAttr: function (good) {
            this._set('good', good);
            this._showGoodPhoto(this.good.photo);
            this._showNewMessages(this.good.newMessages);

            //const description length
            //var descr = this.good.description;
            //var description = descr.slice(0, 50)+" ...";

            this.description.innerHTML = this.good.description;

        },

        postCreate: function () {
        },

        startup: function () {
        },

        _showGoodPhoto: function (goodPhotoUrl) {

            //check good photo
            if (goodPhotoUrl != '') {
                this.goodPhotoNode.src = goodPhotoUrl;
            }
            else {
                this.goodPhotoNode.src = this.defaultGoodPhoto;
            }
        },

        _showNewMessages: function (newMessages) {
            if (newMessages) {
                this.newMessages.innerHTML = newMessages;
                this.baseClass = "present-msgs";
            } else {
                this.baseClass = "absent-msgs";
            }
        },

        /*
         _showNewMessages: function (newMessages) {
         if (newMessages) {
         this.newMessages.innerHTML = newMessages;
         this.fullItem.classList.add('present-msgs');
         } else {
         this.fullItem.classList.add('absent-msgs');
         }
         },
         */
        showSelectedGood: function () {
            //alert("Bla");
            window.location.href = "/ERent/goodFullView.html";
        }

    });
});

