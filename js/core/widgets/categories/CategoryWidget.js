define([
    "dojo/_base/declare",

    "dojo/_base/lang",
    "dojo/dom-style",
    "dojo/mouse",
    "dojo/on",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "core/widgets/categories/CategoryGoodsWidget",

    "dojo/text!./templates/CategoryWidget.html"
], function (declare, lang, domStyle, mouse, on, _WidgetBase, _TemplatedMixin, CategoryGoodsWidget, template) {
    return declare([_WidgetBase, _TemplatedMixin], {

        // A class to be applied to the root node in our template
        baseClass: "categoryWidget",

        // Our template - important!
        templateString: template,

        id: 0,
        _setIdAttr: function () {

        },
        name: "No Name",
        _setNameAttr: function (name) {
            this._set("name", name);
        },

        /**
         * working with categories icons
         */
        categoriesIconsPath: "image/category-icons/",
        iconFormat: ".png",

        categotiesIcons: {},

        imagePath: require.toUrl(this.categoriesIconsPath + "more-categories" + this.iconFormat),


        goods: [],
        showCategoryGoods: function () {
            //  alert("Bla");
            window.location.href = "/ERent/categoryGoods.html" + "?id=" + this.id;
        },

        postCreate: function () {
            // Get a DOM node reference for the root of our widget
            var domNode = this.domNode;
            // Run any parent postCreate processes - can be done at any point
            this.inherited(arguments);

            //set categories icons
            this._setCategoriesIcons();
            this._set("imagePath", this.categotiesIcons[this.id]);
            this.iconNode.src = this.get("imagePath");
        },

        /**
         *set categories icons
         *
         * @private
         */
        _setCategoriesIcons(){
            this.categotiesIcons[1] = this.categoriesIconsPath + "real-estate" + this.iconFormat;
            this.categotiesIcons[2] = this.categoriesIconsPath + "tools" + this.iconFormat;
            this.categotiesIcons[3] = this.categoriesIconsPath + "tourism-and-hobbies" + this.iconFormat;
            this.categotiesIcons[4] = this.categoriesIconsPath + "transport" + this.iconFormat;
            this.categotiesIcons[5] = this.categoriesIconsPath + "electronics" + this.iconFormat;
            this.categotiesIcons[6] = this.categoriesIconsPath + "goods-for-children" + this.iconFormat;
            this.categotiesIcons[7] = this.categoriesIconsPath + "furniture" + this.iconFormat;
            this.categotiesIcons[8] = this.categoriesIconsPath + "clothes-and-shoes" + this.iconFormat;
        },


    });
});