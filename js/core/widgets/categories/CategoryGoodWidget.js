
/**
 * Created by Solomka on 07.11.2016.
 */
define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom",
    "dojo/dom-style",
    "dojo/on",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",

    "dojo/text!./templates/CategoryGoodWidget.html"

], function (declare, lang, dom, domStyle, on, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: template,
        baseClass: "categoryGoodWidget",

        parentWidget: {},

        defaultGoodPhoto: require.toUrl("image/headPhones.jpg"),


        good: {},
        _setGoodAttr: function (good) {
            this._set('good', good);
            this._showGoodPhoto(good.photo);

            //const description length
            //var descr = this.good.description;
            //var description = descr.slice(0, 50)+" ...";

            this.description.innerHTML = this.good.description;

        },

        postCreate: function () {



        },

        startup: function () {

        },

        _showGoodPhoto: function (goodPhotoUrl) {

            //check good photo
            if (goodPhotoUrl != '') {
                this.goodPhotoNode.src = goodPhotoUrl;
            }
            else {
                this.goodPhotoNode.src = this.defaultGoodPhoto;
            }
        },

        showSelectedGood: function () {
            //alert("Bla");

            window.location.href = "/ERent/goodFullView.html" + "?id="+this.good.id;
        }

    });
});

