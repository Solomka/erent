/**
 * Created by Solomka on 01.11.2016.
 */

define([
    "dojo/_base/declare",
    "dojo/request",
    "dojo/_base/lang",
    "dojo/dom-style",
    "dojo/on",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "core/widgets/categories/CategoryGoodWidget",
    "core/widgets/categories/CategoryFilterWidget",

    "dojo/text!./templates/CategoryGoodsWidget.html"

], function (declare, request, lang, domStyle, on, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, CategoryGoodWidget, CategoryFilterWidget, template) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: template,

        parentWidget: {},

        childGoodShortViewWidgets: [],
        categoriesFilterWidget: {},

        goods: [],
        _setGoodsAttr: function (goods) {
            this._set("goods", goods);
        },

        postCreate: function () {
            var categoryGoods = this.goods;
            var widget = {};
            var params = {};

            for (var i in categoryGoods) {

                params = {
                    good: categoryGoods[i],
                    parentWidget: this
                };
                widget = new CategoryGoodWidget(params).placeAt(this.categoryGoods);
                this.childGoodShortViewWidgets.push(widget);

            }

            //add by categories filter
            this._showCategoriesFilter();
        },

        _showCategoriesFilter: function () {
            var widget = {};
            var params = {};

            params = {
                parentWidget: this
            };

            widget = new CategoryFilterWidget(params).placeAt(this.categoryFilter);
            this.categoriesFilterWidget = widget;
        }


    });
});

