/**
 * Created by Solomka on 27.10.2016.
 */

define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom",
    "dojo/dom-style",
    "dojo/on",
    "dojo/query",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "core/widgets/goods/ReviewWidget",
    //"core/models/Good",


    "dojo/text!./templates/GoodFullViewWidget.html"

], function (declare, lang, dom, domStyle, on, query, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, ReviewWidget, template) {
    return declare([_WidgetBase, _TemplatedMixin, ReviewWidget, _WidgetsInTemplateMixin], {

        templateString: template,
        baseClass: "goodFullViewWidget",

        childReviewWidgets: [],
        parentWidget: [],

        defaultPhoto: require.toUrl("image/johnDoeR.png"),
        defaultGoodPhoto: require.toUrl("image/headPhones.jpg"),

        good: {},
        _setGoodAttr: function (good) {
            this._set('good', good);
        },

        postCreate: function () {

            for (var i in this.childReviewWidgets) {
                if (!_.isEmpty(this.childReviewWidgets) && !this.childReviewWidgets[i]._destroyed) {
                    this.childReviewWidgets[i].destroyRecursive();
                }
            }
            var reviews = this.good.reviews;
            //show good reviews
            this._showReviewsWidget(reviews);
            this._showGood(this.good);

        },

        _showGood: function (good) {

            //show Owner and Good photo;
            this._showGoodOwnerPhoto(good.ownerPhoto, good.goodPhoto);


            var revNums = good.reviews.length;

            this.revNum.innerHTML = revNums;
            this.isAvailable.innerHTML = (!!(good.available)) ? "available" : "unavailable";

            //show different divs depending on whether this good belongs to user viewing it
            if(typeof(Storage) !== "undefined") {
                if (localStorage.userId.toString() === good.ownerId.toString()){
                    this._setDisplayByClassName("show-when-mine", "block");
                    this._setDisplayByClassName("show-when-not-mine", "none");
                } else {
                    this._setDisplayByClassName("show-when-not-mine", "block");
                    this._setDisplayByClassName("show-when-mine", "none");
                }
            } else {
                console.log("localStorage unavailable");
            }
        },

        startup: function () {

        },

        //show Owner and Good photo;
        _showGoodOwnerPhoto: function (ownerPhotoUrl, goodPhotoUrl) {

            // check owner photo
            if (ownerPhotoUrl != '') {
                this.ownerPhotoNode.src = ownerPhotoUrl;
            }
            else {
                this.ownerPhotoNode.src = this.defaultPhoto;
            }

            //check good photo
            if (goodPhotoUrl != '') {
                this.goodPhotoNode.src = goodPhotoUrl;
            }
            else {
                this.goodPhotoNode.src = this.defaultGoodPhoto;
            }
        },

        _setDisplayByClassName: function(className, displayValue) {
            var nodeList = this.domNode.getElementsByClassName(className);
            var nodes = Array.prototype.slice.call(nodeList, 0);
            nodes.forEach(function(e){
                e.style.display = displayValue;
            });
        },

        /**
         * Show good reviews
         * @param reviews
         * @private
         */
        _showReviewsWidget: function (reviews) {

            var params = {};
            var review = {};
            var widget = {};

            var reviews = reviews;

            for (var i in reviews) {
                review = reviews[i];

                params = {
                    review: review
                }
                widget = new ReviewWidget(params).placeAt(this.reviewNodes);
                this.childReviewWidgets.push(widget);
            }

        },

        test: function () {
            alert("Click me baby");
        },

        openMessages: function() {
            if(typeof(Storage) !== "undefined") {
                if (localStorage.loggedIn === "true"){
                    alert("You can do it :)")
                    // goto messages.html
                } else {
                    document.getElementById("logInA").click();
                    //alert("Not Logged in");
                }
            } else {
                console.log("localStorage unavailable");
            }
        },

        openGoodDialogs: function(){
            //alert(this.good.id);
            window.location.href = "/ERent/dialogs.html" + "?id="+this.good.id;
        }
    });
});