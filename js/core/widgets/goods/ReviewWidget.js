/**
 * Created by Solomka on 27.10.2016.
 */

define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/dom",
    "dojo/dom-style",
    "dojo/on",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",
    "dojo/text!./templates/ReviewWidget.html"

], function (declare, lang, dom, domStyle, on, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: template,
        baseClass: "reviewViewWidget",


        review: {},
        _setReviewAttr: function (review) {
            if (!_.isEmpty(review)) {
                this._set('review', review);
            }
        },

        postCreate: function () {
            this.showProperReviewTypeStyle(this.review);
        },

        startup: function () {

        },
        /*
         showReview: function (review) {

         this.showProperReviewTypeStyle(review);

         var copy = {};
         lang.mixin(copy, review);


         return lang.replace(this.reviewRepresentation.innerHTML, lang.mixin(copy, {}
         ));
         },
         */
        /**
         * TODO: for Margo
         * this is the method where you have to defined styles
         */

        showProperReviewTypeStyle: function (review) {

            var targetNode = this.domNode;

            switch (review.type) {
                case "positive":
                    domStyle.set(targetNode, "background-color", "#d2f0d2");
                    break;
                case "negative":
                    domStyle.set(targetNode, "background-color", "#f0d2d2");
                    break;
                default:
                    domStyle.set(targetNode, "background-color", "#d2d2d2");
                    break;
            }
        }

    });
});