/**
 * Created by Solomka on 01.11.2016.
 */

define([
    "dojo/_base/declare",
    "dojo/request",
    "dojo/_base/lang",
    "dojo/dom-style",
    "dojo/on",
    "dojo/dom",
    "dojo/query",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",

    "dojo/text!./templates/LoginWidget.html",

    "core/widgets/profile/ProfileDropDownWidget",

    "dijit/form/ValidationTextBox",
    "dijit/form/Form",
    "dijit/form/Button"

], function (declare, request, lang, domStyle, on, dom, query, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template, ProfileDropDownWidget) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: template,
        parentWidget: {},

        postCreate: function () {
            //alert("Login");

        },

        startup: function () {

        },

        /**
         * signIn request
         */
        loginRequest: function () {

            var scope = this;
            //var loginNode = dom.byId("logInA");
            var loginNode = dom.byId("login");
            // var modalNode = dom.byId("signInModal");

            if (this._validateSignInForm()) {

                request("app/data/login.json", {
                        handleAs: "json"
                    }
                ).then(function (data) {
                    if ((data.email == scope.emailInput.get('value')) && (data.password == scope.passInput.get('value'))) {

                        // console.log(query('#signInModal')[0]);

                        /*
                         Header.user = {
                         email: data.email,
                         password: data.password
                         };
                         */
                        //Storage.memoryStore.put(data);

                        //alert(Storage.memoryStore.get(1));

                        //empty input fields
                        /*
                         scope.emailInput.set('value', "");
                         scope.passInput.set('value', "");
                         scope.emailInput.set('active', false);
                         scope.passInput.set('active', false);
                         */
                        //close modal
                        if (typeof(Storage) !== "undefined") {
                            localStorage.loggedIn = "true";
                            localStorage.userId = data.id;
                        } else {
                            console.log("localStorage unavailable");
                        }

                        scope.closeModal.click();

                        // hide login modal on click
                        domStyle.set(Header.login, "display", "none");

                        var params = {
                            parentWidget: scope
                        };

                        if (!!(Header.childProfileWidget) && !_.isEmpty(Header.childProfileWidget) && !Header.childProfileWidget._destroyed) {
                            Header.childProfileWidget.destroyRecursive();
                        }

                        var widget = new ProfileDropDownWidget(params).placeAt(Header.profile);
                        Header.childProfileWidget = widget;
                        domStyle.set(Header.profile, "display", "initial");

                        //loginNode.innerHTML = "<i class='fa fa-sign-out' aria-hidden='true'></i>";

                        /*
                         require(["dojo/text!views/profileMenu.html"], function (menu) {
                         loginNode.innerHTML = menu;
                         }); */
                    }


                });

            }
            else {
                this.emailInput.set('value', "");
                this.passInput.set('value', "");
                domStyle.set(this.invalidInputMessage, "display", "inline");
            }
        },

        /**
         * signUp request
         */
        signUpRequest: function () {

            if (this._validateSignUpForm()) {
                /**
                 * signIn request goes here
                 */
                /*
                 request("app/data/login.json", {
                 handleAs: "json"
                 }).then(function (data) {
                 });  */
            }
            else {
                this.emailSignUpInput.set('value', "");
                this.usernameInput.set('value', "");
                this.passSignUpInput.set('value', "");
                this.passRepeatSignUpInput.set('value', "");
                domStyle.set(this.invalidInputMessageSignUp, "display", "inline");
            }
        },

        /**
         * Validation for signIn form
         * @returns {boolean}
         * @private
         */

        _validateSignInForm: function () {
            return !!( this.emailInput.isValid() && this.passInput.isValid() );
        },

        /**
         * Validation for SignUp form
         * @returns {boolean}
         * @private
         */
        _validateSignUpForm: function () {
            return !!( this.emailSignUpInput.isValid() && this.usernameInput.isValid() && this.passSignUpInput.isValid() && this.passRepeatSignUpInput.isValid() );
        }


    });
});

