/**
 * Created by Solomka on 17.11.2016.
 */

/**
 * Created by Solomka on 01.11.2016.
 */

define([
    "dojo/_base/declare",
    "dojo/request",
    "dojo/_base/lang",
    "dojo/dom-style",
    "dojo/on",
    "dojo/dom",
    "dojo/query",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",

    "core/widgets/profile/ProfileEditWidget",

    "dojo/text!./templates/ProfileWidget.html"

], function (declare, request, lang, domStyle, on, dom, query, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, ProfileEditWidget, template) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: template,
        parentWidget: {},
        childEditWidget: {},

        postCreate: function () {
            // alert("Profile");

        },

        startup: function () {

        },

        editProfile: function () {

            domStyle.set(this.profileNode, "display", "none");
            var widget = new ProfileEditWidget().placeAt(this.profileEditNode);
            childEditWidget = widget;


        }


    });
});


