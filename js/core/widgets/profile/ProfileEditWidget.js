/**
 * Created by Solomka on 17.11.2016.
 */


define([
    "dojo/_base/declare",
    "dojo/request",
    "dojo/_base/lang",
    "dojo/dom-style",
    "dojo/on",
    "dojo/dom",
    "dojo/query",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",

    "dojo/text!./templates/ProfileEditWidget.html"

], function (declare, request, lang, domStyle, on, dom, query, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: template,
        parentWidget: {},

        postCreate: function () {
            // alert("Profile");

        },

        startup: function () {

        }


    });
});


