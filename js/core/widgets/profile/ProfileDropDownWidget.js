/**
 * Created by Solomka on 01.11.2016.
 */

define([
    "dojo/_base/declare",
    "dojo/request",
    "dojo/_base/lang",
    "dojo/dom-style",
    "dojo/on",
    "dojo/dom",
    "dojo/query",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",
    "dijit/_WidgetsInTemplateMixin",

    "dojo/text!./templates/ProfileDropDownWidget.html"

], function (declare, request, lang, domStyle, on, dom, query, _WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin, template) {
    return declare([_WidgetBase, _TemplatedMixin, _WidgetsInTemplateMixin], {

        templateString: template,
        parentWidget: {},

        postCreate: function () {
            // alert("Profile");

        },

        startup: function () {
            // myFormDialog.show();
        },

        logOut: function () {
            //Header.user = null;
            Storage.memoryStore.remove(1);
            domStyle.set(Header.profile, "display", "none");
            domStyle.set(Header.login, "display", "initial");
            if(typeof(Storage) !== "undefined") {
                localStorage.loggedIn = false;
                localStorage.userId = null;
            } else {
                alert("localStorage unavailable");
            }
            window.location.href = "/ERent/main.html";
        }


    });
});

