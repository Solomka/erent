/**
 * Created by Solomka on 01.11.2016.
 */

define([
    "dojo/_base/declare",
    "dojo/ready",
    "dojo/request",
    "dojo/_base/lang",
    "dojo/on",
    "dojo/dom",
    "dojo/dom-style",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/_base/array",

    "dijit/Dialog",
    "core/widgets/login/LoginWidget",
    "core/widgets/profile/ProfileDropDownWidget",
    "core/storage/Storage",

    "dojo/domReady!"

], function (declare, ready, request, lang, on, dom, domStyle, domClass, domConstruct, arrayUtil, Dialog, LoginWidget, ProfileDropDownWidget, Storage) {

    /**
     * Controller for head page
     * @type {{rent: DOMNode, search: DOMNode, categories: DOMNode, contacts: DOMNode, login: DOMNode, childLoginWidget: {}, init: dojo.global.Header.init}}
     */
    dojo.global.Header = {

        //user: null,

        rent: dom.byId("rent"),
        search: dom.byId("search"),
        categories: dom.byId("categories"),
        contacts: dom.byId("contacts"),

        login: dom.byId("login"),
        profile: dom.byId("profile"),

        childLoginWidget: {},
        childProfileWidget: {},

        init: function () {

            /**
             * show Contacts and Categories in Header if main.html
             */

            //alert(document.location.pathname);
            if (document.location.pathname == "/ERent/main.html") {

                /*
                 domConstruct.create("a", {
                 href: "#l-categories",
                 class: "scroll-link",
                 onclick: "makeSelected(this)",
                 innerHTML: "Categories"
                 }, this.categories);

                 domConstruct.create("a", {
                 href: "#l-contacts",
                 class: "scroll-link",
                 onclick: "makeSelected(this)",
                 innerHTML: "Contacts"
                 }, this.contacts);
                 */

                domConstruct.place("<a href='#l-categories' class='scroll-link header-a' data-id='categories'" +
                    ">Categories</a>", this.categories);

                domConstruct.place("<a href='#l-contacts' class='scroll-link header-a' data-id='contacts'" +
                    "'>Contacts</a>", this.contacts);

                //domClass.add(this.contacts, "show");
                //domClass.add(this.categories, "show");

                //domStyle.set(this.contacts, "display", "inline-bl");
                //domStyle.set(this.categories, "display", "inline");
            }

            // check if localStorage is available
            if(typeof(Storage) !== "undefined") {
                console.log('localStorage available');
                if (localStorage.loggedIn === "true"){
                    var params = {};
                    if (!!(this.childProfileWidget) && !_.isEmpty(this.childProfileWidget) && !this.childProfileWidget._destroyed) {
                        this.childProfileWidget.destroyRecursive();
                    }

                    var widget = new ProfileDropDownWidget(params).placeAt(this.profile);
                    this.childProfileWidget = widget;
                    domStyle.set(this.profile, "display", "initial");
                } else {
                    domConstruct.place("<a data-toggle='modal' id='logInA' href='#signInModal' class='header-a'>" +
                        "<i class='fa fa-sign-in' aria-hidden='true'></i></a>", this.login);
                    domStyle.set(this.profile, "display", "none");
                }
            } else {
                console.log('localStorage unavailable');
            }

            var scope = this;

            on(this.login, "click", function () {

               // if (scope.user === null) {
                if (_.isEmpty(Storage.memoryStore.data)) {

                    if (!!(scope.childLoginWidget) && !_.isEmpty(scope.childLoginWidget) && !scope.childLoginWidget._destroyed) {
                        scope.childLoginWidget.destroyRecursive();
                    }
                    var params = {
                        parentWidget: scope
                    }

                    var widget = new LoginWidget(params).placeAt(dom.byId("loginForm"));
                    scope.childLoginWidget = widget;
                }

                else {
                    // DROPDOWN
                }

            });

        }

    };

    ready(function () {
        Header.init();
    });

    return dojo.global.Header;
});
