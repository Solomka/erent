/**
 * Created by Solomka on 27.10.2016.
 */

define([
    "dojo/_base/declare",
    "dojo/ready",
    "dojo/request",
    "dojo/_base/lang",
    "dojo/on",
    "dojo/dom",
    "dojo/dom-style",
    "dojo/_base/array",
    "dojo/promise/all",

    "core/widgets/categories/CategoryWidget",
    "core/widgets/goods/GoodShortViewWidget",

    "dojo/domReady!"

], function (declare, ready, request, lang, on, dom, domStyle, arrayUtil,all, CategoryWidget, GoodShortViewWidget) {

    /**
     * Controller for main page
     *
     * @type {{childCategoryWidgets: Array, init: dojo.global.Main.init, _loadGoodCategories: dojo.global.Main._loadGoodCategories, showSelectedGood: dojo.global.Main.showSelectedGood}}
     */
    dojo.global.Main = {

        childCategoryWidgets: [],
        childPopularItemWidgets: [],


        init: function () {

            var scope = this;
            this._loadPopularItemsCategories();

        },


        /**
         * Load good categories
         * @private
         */
        /*
        _loadPopularItemsCategories: function () {
            var scope = this;

            // Get references to our containers
            var categoryContainer = dom.byId("categoryContainer");
            var popItemsContainer = dom.byId("popularItems");

            request("app/data/categoriesPopItems.json", {
                handleAs: "json"
            }).then(function (data) {

                //Create CategoryWidget for each category
                arrayUtil.forEach(data.categories, function (category) {
                    // Create our widget and place it
                    var widget = new CategoryWidget(category).placeAt(categoryContainer);
                    scope.childCategoryWidgets.push(widget);
                });

                //Create GoodShortViewWidget for each popular item
                arrayUtil.forEach(data.popularItems, function (popularItem) {

                    var params = {
                        good: popularItem,
                        parentWidget: scope
                    }
                    // Create our widget and place it
                    var widget = new GoodShortViewWidget(params).placeAt(popItemsContainer);
                    scope.childPopularItemWidgets.push(widget);
                });
            });

        },*/

        _loadPopularItemsCategories: function(){
            var scope = this;

            // Get references to our containers
            var categoryContainer = dom.byId("categoryContainer");
            var popItemsContainer = dom.byId("popularItems");

            /*
            var categoriesPromise = request.get("https://dev.erent.online/api/categories");
            var popularItemsPromise = request.get("https://dev.erent.online/api/things?limit=3");
*/
            var categoriesPromise = request.get("app/data/categories.json", {
                handleAs: "json"
            });
            var popularItemsPromise = request.get("app/data/categoriesPopItems.json", {
                handleAs: "json"
            });

            all({
                categories: categoriesPromise,
                popularItems: popularItemsPromise
            }).then(function(results){
                // results will be an Object using the keys "promise1" and "promise2"

                //Create CategoryWidget for each category
                arrayUtil.forEach(results.categories, function (category) {
                    // Create our widget and place it
                    var widget = new CategoryWidget(category).placeAt(categoryContainer);
                    scope.childCategoryWidgets.push(widget);
                });

                //Create GoodShortViewWidget for each popular item
                arrayUtil.forEach(results.popularItems.popularItems, function (popularItem) {

                    var params = {
                        good: popularItem,
                        parentWidget: scope
                    }
                    // Create our widget and place it
                    var widget = new GoodShortViewWidget(params).placeAt(popItemsContainer);
                    scope.childPopularItemWidgets.push(widget);
                });
            });

        }


    };

    ready(function () {
        Main.init();
    });

    return dojo.global.Main;
});