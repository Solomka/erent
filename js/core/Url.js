/**
 * Created by Solomka on 29.11.2016.
 */

define([
    "dojo/_base/declare",
    "dojo/ready",
    "dojo/request",
    "dojo/_base/lang",
    "dojo/on",
    "dojo/dom",
    "dojo/dom-style",
    "dojo/_base/array",
    "dojo/io-query",

    "dojo/domReady!"

], function (declare, ready, request, lang, on, dom, domStyle, arrayUtil, ioQuery) {

    dojo.global.Url = {

        init: function () {
        },

        getUrlParams: function () {
            var urlParams = window.location.search;
            return urlParams.substring(urlParams.indexOf("?") + 1, urlParams.length);
        },

        getUrlParamsAsObject: function () {
            var query = this.getUrlParams();
            return ioQuery.queryToObject(query);
        }
    };

    ready(function () {
        Url.init();
    });

    return dojo.global.Url;
});
