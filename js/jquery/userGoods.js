/**
 * Created by marvich on 17.11.2016.
 */
document.addEventListener("DOMContentLoaded", function (event) {
    var _selector = document.querySelector('input[name=onlyNew]');
    _selector.addEventListener('change', function (event) {
        var nodeList = document.getElementsByClassName('absent-msgs');
        var nodes = Array.prototype.slice.call(nodeList, 0);
        if (_selector.checked) {
            nodes.forEach(function(e){
                // hide goods that have no new messages
                e.style.display = 'none';
            });
        } else {
            nodes.forEach(function(e){
                // show goods that have no new messages
                e.style.display = 'block';
            });
        }
    });
});