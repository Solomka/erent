/**
 * Created by marvich on 24.11.2016.
 */

// preview selected photo on form page
function preview(input) {
    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {
            $('#goodPhoto')
                .attr('src', e.target.result)
        };

        reader.readAsDataURL(input.files[0]);
    }
}
