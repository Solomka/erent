/**
 * Created by marvich on 16.11.2016.
 */
$(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [label]);
});
$(document).ready( function() {
    $(':file').on('fileselect', function(event, label) {
        $("#filename").html(label);
        $("#file-alert").show();
    });
});
