/**
 * Created by marvich on 17.11.2016.
 */

function showSignUpForm(){
    document.getElementById('signInForm').style.display = 'none';
    document.getElementById('signUpForm').style.display = 'block';
    document.getElementById('modal-title').innerHTML = "Sign Up";
}

function showSignInForm(){
    document.getElementById('signUpForm').style.display = 'none';
    document.getElementById('signInForm').style.display = 'block';
    document.getElementById('modal-title').innerHTML = "Sign In";
}

